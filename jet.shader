/*
	Jet Shader by あるる（きのもと 結衣） @arlez80

	MIT License
*/

shader_type spatial;
render_mode unshaded, cull_back, blend_mix, depth_draw_never;

// 根本と先っちょの色
uniform vec4 first_color : hint_color = vec4( 0.866666666, 1.0, 0.0, 1.0 );
uniform vec4 last_color : hint_color = vec4( 0.0, 1.0, 1.0, 1.0 );
// 0.0 <= fire <= 1.0
uniform float fire : hint_range( 0.0, 1.0 ) = 1.0;

void fragment( )
{
	float t = UV.y;
	float s = 1.0 - t;

	// 色
	ALBEDO = first_color.rgb * s + last_color.rgb * t;

	// ギザギザつきアルファ
	float PI = 3.1415926;
	ALPHA = min(
		0.3,
		max(
			0.0,
			// 後方につれてだんだんと薄くなる
			s
			// 大きいなみ
		+	sin(UV.x * PI * 8.123456 + TIME * 42.15) * 0.09
			// 小さななみ
		+	sin(UV.x * PI * 42.987654 - TIME * 34.12345) * 0.05
			// fireによる大きさ制御
		-	1.14 + fire
		)
	);
}
